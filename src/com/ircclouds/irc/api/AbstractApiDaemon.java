package com.ircclouds.irc.api;

import java.io.*;

import org.bukkit.Bukkit;
import com.ircclouds.irc.api.comms.IConnection.EndOfStreamException;
import com.ircclouds.irc.api.domain.messages.interfaces.*;
import com.ircclouds.irc.api.filters.*;

public abstract class AbstractApiDaemon extends Thread
{

	private IMessageReader reader;
	private IMessageDispatcher dispatcher;

	public AbstractApiDaemon(IMessageReader aReader, IMessageDispatcher aDispatcher)
	{
		super("ApiDaemon");

		reader = aReader;
		dispatcher = aDispatcher;
	}

	public void run()
	{
		try
		{
			while (reader.available())
			{
				IMessage _msg = reader.readMessage();
				if (_msg != IMessage.NULL_MESSAGE)
				{
					dispatcher.dispatchToPrivateListeners(_msg);
					
					if (getMessageFilter() != null)
					{
						MessageFilterResult _fr = getMessageFilter().filter(_msg);
						if (_fr.getFilterStatus().equals(FilterStatus.PASS))
						{
							dispatcher.dispatch(_fr.getFilteredMessage(), getMessageFilter().getTargetListeners());
						}
					}
					else
					{
						dispatcher.dispatch(_msg, TargetListeners.ALL);
					}
				}
			}
		}
		catch (EndOfStreamException aExc)
		{
			Bukkit.getLogger().severe(aExc.getLocalizedMessage());
		}
		catch (IOException aExc)
		{
			Bukkit.getLogger().severe(aExc.getLocalizedMessage());
			
			signalExceptionToApi(aExc);
		}
		finally
		{
			onExit();
		}
	}

	protected abstract void signalExceptionToApi(Exception aExc);

	protected abstract void onExit();
	
	protected abstract IMessageFilter getMessageFilter();
}
