package com.ircclouds.irc.api;

import java.io.*;

import org.bukkit.Bukkit;
import com.ircclouds.irc.api.commands.*;
import com.ircclouds.irc.api.comms.*;

public abstract class AbstractCommandServerImpl implements ICommandServer, INeedsConnection
{
	public void execute(ICommand aCommand) throws IOException
	{

		String _str = aCommand.asString() + "\r\n";
		int _written = getConnection().write(_str);
		if (_str.length() > _written)
		{
			Bukkit.getLogger().severe("Expected to write " + _str.length() + " bytes, but wrote " + _written);
		}
	}
}
