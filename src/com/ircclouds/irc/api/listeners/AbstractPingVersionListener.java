package com.ircclouds.irc.api.listeners;

import java.io.*;

import org.bukkit.Bukkit;

import com.ircclouds.irc.api.*;
import com.ircclouds.irc.api.commands.*;
import com.ircclouds.irc.api.domain.messages.*;

public abstract class AbstractPingVersionListener extends VariousMessageListenerAdapter
{

	@Override
	public void onServerPing(ServerPing aMsg)
	{
		execute(new SendServerPingReplyCmd(new ServerPongMessage(aMsg.getText())));
	}

	protected abstract IIRCSession getSession();
	
	private void execute(ICommand aCmd)
	{
		try
		{
			getSession().getCommandServer().execute(aCmd);
		}
		catch (IOException aExc)
		{
			Bukkit.getLogger().severe(
					"Error Executing Command [" + aCmd.asString() + "]: " + aExc.getLocalizedMessage());
		}
	}
}
