package net.ondryaso.plugins.HermanIRC;

import java.text.Normalizer;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import net.ondryaso.plugins.HermanIRC.irc.ComBase;

public class HIHandler extends Handler {
	ComBase irc;
	
	public HIHandler(ComBase irc) {
		this.irc = irc;
	}

	@Override
	public void close() throws SecurityException {

	}

	@Override
	public void flush() {

	}

	@Override
	public void publish(LogRecord arg0) {

		try {
			String s = "[%1] %2"
					.replace("%1", arg0.getLevel().toString())
					.replace("%2", arg0.getMessage());
			String norm = Normalizer.normalize(s, Normalizer.Form.NFD)
					.replaceAll("[^\\p{ASCII}]", "");
			
			irc.getConnection().message(HIMain.channelName, norm);
			
		} catch (Exception e) {
			
		}

	}

}
