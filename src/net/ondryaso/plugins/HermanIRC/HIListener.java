package net.ondryaso.plugins.HermanIRC;

import java.text.Normalizer;

import net.ondryaso.plugins.HermanIRC.irc.ComBase;

import org.bukkit.Server;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class HIListener implements Listener {
	Server s;
	ComBase irc;

	public HIListener(Server server, ComBase irc) {
		s = server;
		this.irc = irc;
	}

	@EventHandler
	public void onChat(org.bukkit.event.player.AsyncPlayerChatEvent c) {
		irc.getConnection().message(
				HIMain.channelName,
				HIMain.chatFormat.replace("%s", c.getPlayer().getName())
						.replace("%m",
								Normalizer.normalize(c.getMessage(),
										Normalizer.Form.NFD).replaceAll(
										"[^\\p{ASCII}]", "")));
	}

	@EventHandler
	public void onLogin(PlayerJoinEvent e) {
		irc.getConnection()
				.message(
						HIMain.channelName,
						HIMain.connectMsg.replace("%p", e.getPlayer()
								.getDisplayName()));
	}

	@EventHandler
	public void onDisconnect(PlayerQuitEvent e) {
		irc.getConnection().message(
				HIMain.channelName,
				HIMain.disconnectMsg.replace("%p", e.getPlayer()
						.getDisplayName()));
	}

}
