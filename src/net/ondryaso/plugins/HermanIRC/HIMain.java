package net.ondryaso.plugins.HermanIRC;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.Normalizer;
import java.util.ArrayList;
import net.ondryaso.plugins.HermanIRC.irc.ComBase;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import com.ircclouds.irc.api.Callback;
import com.ircclouds.irc.api.domain.IRCUser;
import com.ircclouds.irc.api.state.IIRCState;

public class HIMain extends JavaPlugin {

	// IRC server [irc]
	public static String ircServer;

	// IRC server port [irc]
	public static int ircPort;

	// IRC encoding [irc]
	public static String ircEncoding;

	// Bot name [irc]
	public static String botName;

	// NICKSERV [irc]
	public static String ircPassword;

	// Channel to join [irc]
	public static String channelName;

	// Channel password
	public static String channelPassword;

	// OPs from IRC [irc]
	public static ArrayList<String> ircAdmins = new ArrayList<String>();

	// Not admin error [irc]
	public static String adminErrorMsg;

	// Connect message [game->irc]
	public static String connectMsg;

	// Disconnect message [game->irc]
	public static String disconnectMsg;

	// Send everything from IRC [irc->game]
	public static boolean sendAll;

	// Send everything from console [game->irc]
	public static boolean fullsync;

	// IRC message format [game]
	public static String msgFormat;

	// Game message format [irc]
	public static String chatFormat;

	private ComBase irc;
	private HIListener l;

	public void onDisable() {
		irc.getConnection().disconnect("Server is shutting down.");
	}

	public void onEnable() {

		saveDefaultConfig();
		loadConfig();

		irc = new ComBase(botName, ircServer, ircPort, channelName,
				ircEncoding, ircPassword, channelPassword);

		final org.bukkit.plugin.Plugin p = this;
		irc.initialize(new Callback<IIRCState>() {

			@Override
			public void onSuccess(IIRCState aObject) {
				doCommands();
			}

			@Override
			public void onFailure(Exception aExc) {
				getLogger().warning("Error while connecting to IRC server!");
				getPluginLoader().disablePlugin(p);
			}

		});

		// Load in-game listener
		l = new HIListener(getServer(), irc);
		getServer().getPluginManager().registerEvents(l, this);

		// If server should send everything from console, register console
		// handler
		if (fullsync) {
			HIHandler h = new HIHandler(irc);
			this.getServer().getLogger().addHandler(h);
		}
	}

	// Do IRC commands
	public void doCommands() {
		File loginCommands = new File(this.getDataFolder(), "commands.txt");
		if (!loginCommands.exists()) {
			try {
				this.getLogger().info("Creating commands file");
				loginCommands.createNewFile();
			} catch (IOException e) {
				this.getLogger().warning("Can't create commands file!");
			}
		}

		FileInputStream fs = null;
		DataInputStream in = null;
		BufferedReader br = null;

		try {
			fs = new FileInputStream(loginCommands);
			in = new DataInputStream(fs);
			br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				irc.getConnection().rawMessage(strLine);
			}
		} catch (IOException e) {
			this.getLogger().warning("Can't read commands file!");
		} finally {
			try {
				
				if (br != null)
					br.close();
				if (in != null)
					in.close();
				if (fs != null)
					fs.close();
				
			} catch (IOException e) {
				this.getLogger().severe(e.getLocalizedMessage());
			}
		}
	}

	private String join(String[] array, int startIndex, String joiningString) {
		String ret = "";
		int counter = 0;
		for (String s : array) {
			if (counter != startIndex) {
				counter++;
				continue;
			}

			ret += s + joiningString;
		}

		return ret;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (command.getName().equalsIgnoreCase("ircmd")) {
			// Send raw irc command
			if (args.length >= 1) {
				irc.getConnection().rawMessage(this.join(args, 0, " "));
			}

		} else if (command.getName().equalsIgnoreCase("irclist")) {
			// List users on IRC
			String ret = ChatColor.AQUA + "Users on IRC: " + ChatColor.WHITE;

			for (IRCUser u : irc.getIRCState()
					.getChannelByName(HIMain.channelName).getUsers()) {
				ret += u.getNick() + "; ";
			}

			sender.sendMessage(ret);

		}
		return true;
	}

	public static String stringToGameFormat(String s, String sender) {
		String norm = Normalizer.normalize(s, Normalizer.Form.NFD).replaceAll(
				"[^\\p{ASCII}]", "");
		return ChatColor.GREEN
				+ msgFormat.replace("%s", sender).replace("%m",
						ChatColor.WHITE + norm);
	}

	private void loadConfig() {
		FileConfiguration config = getConfig();
		botName = config.getString("BotName");
		channelName = "#" + config.getString("ChannelName");
		ircAdmins = (ArrayList<String>) config.getStringList("IrcAdmins");
		ircServer = config.getString("IrcServer");
		adminErrorMsg = config.getString("AdminErrorMsg");
		ircPort = config.getInt("IrcPort");
		ircEncoding = config.getString("Encoding");
		connectMsg = config.getString("UserConnectMsg");
		disconnectMsg = config.getString("UserDisconnectMsg");
		sendAll = config.getBoolean("SendAllMessages");
		fullsync = config.getBoolean("FullConsoleSync");
		msgFormat = config.getString("MessageFormat");
		chatFormat = config.getString("ChatFormat");
		ircPassword = config.getString("RegisteredPassword");
		channelPassword = config.getString("ChannelPassword");
	}

}
