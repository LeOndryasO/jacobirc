package net.ondryaso.plugins.HermanIRC.irc;

import java.util.ArrayList;
import java.util.List;

import com.ircclouds.irc.api.Callback;
import com.ircclouds.irc.api.IRCApi;
import com.ircclouds.irc.api.IRCApiImpl;
import com.ircclouds.irc.api.IServerParameters;
import com.ircclouds.irc.api.domain.IRCServer;
import com.ircclouds.irc.api.state.IIRCState;


public class ComBase {
	IRCApi irc = new IRCApiImpl(true);
	IServerParameters params;
	IIRCState state;
	String _chToJoin;
	String _chPass;
	String _nsPass;
	
	public ComBase(String nick, String server, int port, String channel) {
		this(nick, server, port, channel, "utf-8", null, null);
	}
	
	public ComBase(final String nick, final String server, final int port, String channel, 
			String encoding, String nspass, String chpass) {
		params = new IServerParameters() {

			@Override
			public String getNickname() {
				return nick;
			}

			@Override
			public List<String> getAlternativeNicknames() {
				ArrayList<String> alts = new ArrayList<String>(3);
				alts.add(nick + "_1");
				alts.add("JACOBIRC_" + nick);
				return alts;
			}

			@Override
			public String getIdent() {
				return "JacobIRCv4";
			}

			@Override
			public String getRealname() {
				return "JacobIRC Bot v4";
			}

			@Override
			public IRCServer getServer() {
				return new IRCServer(server, port);
			}
			
		};
		
		
		irc.addListener(new MessageAdapter());
		irc.addListener(new CommandsAdapter(irc));
		_chToJoin = channel;
		_chPass = chpass;
		_nsPass = nspass;
	}
	
	public IRCApi getConnection() {
		return irc;
	}
	
	public IIRCState getIRCState() {
		return state;
	}
	
	public void initialize(final Callback<IIRCState> callback) {
		irc.connect(params, new Callback<IIRCState>() {

			@Override
			public void onSuccess(IIRCState aObject) {
				state = aObject;
				
				if(_nsPass != null)
					irc.message("NICKSERV", "IDENTIFY " + _nsPass);
				
				if(_chPass != null)
					irc.joinChannel(_chToJoin, _chPass);
				else
					irc.joinChannel(_chToJoin);
				
				if(callback != null)
					callback.onSuccess(aObject);
			}

			@Override
			public void onFailure(Exception aExc) {
				if(callback != null)
					callback.onFailure(aExc);
			}
			
		});
		
	}
}
