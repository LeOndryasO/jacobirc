package net.ondryaso.plugins.HermanIRC.irc;

import net.ondryaso.plugins.HermanIRC.HIMain;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;

import com.ircclouds.irc.api.IRCApi;
import com.ircclouds.irc.api.domain.messages.ChannelPrivMsg;
import com.ircclouds.irc.api.listeners.VariousMessageListenerAdapter;
public class CommandsAdapter extends VariousMessageListenerAdapter {
	Server s;
	IRCApi irc;
	
	public CommandsAdapter(IRCApi irc) {
		s = Bukkit.getServer();
		this.irc = irc;
	}

	@Override
	public void onChannelMessage(ChannelPrivMsg aMsg) {
		if (aMsg.getText().startsWith("!players")) {
			String ret = "Online: ";
			for (Player p : s.getOnlinePlayers()) {
				ret += p.getDisplayName() + "; ";
			}

			irc.message(aMsg.getChannelName(), ret);
		}

		if (aMsg.getText().startsWith("!cmd ")) {
			if (HIMain.ircAdmins.contains(aMsg.getSource().getNick())) {
				String cmd = aMsg.getText().replace("!cmd ", "");
				s.dispatchCommand(s.getConsoleSender(), cmd);
			} else {
				irc.message(aMsg.getChannelName(), HIMain.adminErrorMsg);
			}
		}
	}
}
