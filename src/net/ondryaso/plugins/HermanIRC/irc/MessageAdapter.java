package net.ondryaso.plugins.HermanIRC.irc;

import org.bukkit.Bukkit;
import org.bukkit.Server;


import com.ircclouds.irc.api.domain.messages.ChannelPrivMsg;
import com.ircclouds.irc.api.listeners.VariousMessageListenerAdapter;

import net.ondryaso.plugins.HermanIRC.HIMain;

public class MessageAdapter extends VariousMessageListenerAdapter {
	Server s;
	
	public MessageAdapter() {
		s = Bukkit.getServer();
	}
	
	
	
	@Override
	public void onChannelMessage(ChannelPrivMsg aMsg) {
		if(HIMain.sendAll) {
			s.broadcastMessage(HIMain.stringToGameFormat(aMsg.getText(), aMsg.getSource().getNick()));
		} else {
			String neededWord = HIMain.botName + ":";
			if(aMsg.getText().startsWith(neededWord)) {
				String crop = aMsg.getText().substring(neededWord.length()).trim();
				s.broadcastMessage(HIMain.stringToGameFormat(crop, aMsg.getSource().getNick()));
			}
		}
	}

}
